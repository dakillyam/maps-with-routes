package com.example.myapplication

data class EndLocation(
    val lat: Double,
    val lng: Double
)