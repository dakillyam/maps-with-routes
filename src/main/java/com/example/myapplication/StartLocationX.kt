package com.example.myapplication

data class StartLocationX(
    val lat: Double,
    val lng: Double
)