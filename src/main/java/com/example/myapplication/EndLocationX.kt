package com.example.myapplication

data class EndLocationX(
    val lat: Double,
    val lng: Double
)