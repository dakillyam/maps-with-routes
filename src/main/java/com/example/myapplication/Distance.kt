package com.example.myapplication

data class Distance(
    val text: String,
    val value: Int
)