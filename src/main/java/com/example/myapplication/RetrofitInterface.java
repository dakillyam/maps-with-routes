package com.example.myapplication;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface RetrofitInterface {
    @POST("/maps/api/directions/json")
    Call<RouteResponse> getRoute(
        @Query("origin")  String startplace,
        @Query("destination") String endplace,
        @Query("key") String key,
        @Query("language") String language,
        @Query("mode") String routemode
    );
}
