package com.example.myapplication

data class Duration(
    val text: String,
    val value: Int
)