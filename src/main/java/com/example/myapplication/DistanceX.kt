package com.example.myapplication

data class DistanceX(
    val text: String,
    val value: Int
)