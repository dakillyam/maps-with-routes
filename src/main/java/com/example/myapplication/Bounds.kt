package com.example.myapplication

data class Bounds(
    val northeast: Northeast,
    val southwest: Southwest
)