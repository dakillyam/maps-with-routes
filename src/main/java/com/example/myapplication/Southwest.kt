package com.example.myapplication

data class Southwest(
    val lat: Double,
    val lng: Double
)