package com.example.myapplication

data class DurationX(
    val text: String,
    val value: Int
)