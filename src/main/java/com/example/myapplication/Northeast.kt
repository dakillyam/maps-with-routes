package com.example.myapplication

data class Northeast(
    val lat: Double,
    val lng: Double
)