package com.example.myapplication

data class StartLocation(
    val lat: Double,
    val lng: Double
)